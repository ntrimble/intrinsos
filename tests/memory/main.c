#include <stdio.h>

#include "../../src/IntOS.h"

/* Prototypes */
int t1task(void);
int t2task(void);
int t3task(void);
int t4task(void);

/* Main */
int main(void) {

        /* New RTOS system */
        IO_RTOS_STATE OS;
        IO_RTOS_STATE* pOS = &OS;
        pOS->threadCount = 0;

        /* Register threads with RTOS */
        IO_ADD_THREAD(pOS, t1task, PRIORITY_CRITICAL, 20);
        IO_ADD_THREAD(pOS, t2task, PRIORITY_LOW, 50);
        IO_ADD_THREAD(pOS, t3task, PRIORITY_MEDIUM, 34);
        IO_ADD_THREAD(pOS, t4task, PRIORITY_CRITICAL, 40);
        //IO_ADD_THREAD(pOS, t1task, PRIORITY_CRITICAL,   20);
        //IO_ADD_THREAD(pOS, t2task, PRIORITY_HIGH,       50);
        //IO_ADD_THREAD(pOS, t3task, PRIORITY_LOW,        34);
        //IO_ADD_THREAD(pOS, t4task, PRIORITY_CRITICAL,   40);

        IO_THREAD_STATUS(pOS);

        /* Init. RTOS */
        IO_INIT(pOS);

        IO_THREAD_STATUS(pOS);

        /* Begin */
        IO_MAIN(pOS);

        //IO_THREAD_STATUS(pOS);

        /* After RTOS exits, tidy up */ 
        
        IO_DELETE_THREAD(pOS, pOS->pThreads[0]);
        IO_DELETE_THREAD(pOS, pOS->pThreads[1]);
        IO_DELETE_THREAD(pOS, pOS->pThreads[2]);
        IO_DELETE_THREAD(pOS, pOS->pThreads[3]);
        //IO_DELETE_THREAD(pOS, pOS->pThreads[4]);
        //IO_DELETE_THREAD(pOS, pOS->pThreads[5]);
        //IO_DELETE_THREAD(pOS, pOS->pThreads[6]);
        //IO_DELETE_THREAD(pOS, pOS->pThreads[7]);
           
        return 0;
}

int t1task() {
        printf("[RUNTIME] Task1\n");
        return 0;
}

int t2task() {
        printf("[RUNTIME] Task2\n");
        return 0;
}

int t3task() {
        printf("[RUNTIME] Task3\n");
        return 0;
}

int t4task() {
        printf("[RUNTIME] Task4\n");
        return 0;
}