#include <stdlib.h>
#include <stdio.h>

#include "bst.h"

/* Prototypes */

void displayTree(bst_node**);

/* Variables */

comparator_t test;

int main(void) {

	/* Start binary tree with root node */
	int* rootValue = (int*)malloc(sizeof(int));
	*rootValue = 99;
	bst_node* btree = new_node(rootValue);

	/* Add a child */
	//int* valueA = (int*)malloc(sizeof(int));
	//*valueA = 1;
	//insert(btree,test,valueA);
	
	displayTree(btree);

	return 0;
}

void displayTree(bst_node** start) {

	bst_node** node = start;
	printf("\n\nROOT: %d   ", &(*node)->data);

	while(node != NULL) {
		node = &(*node)->left;
	}

}