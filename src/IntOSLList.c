/* 
 * IntrinsOS Linked List (ll) abstract data type
 *
 */

#include "IntOSLList.h"
#include <stdlib.h>

llist* new_node(void* x, struct llist* nextNode, struct llist* prevNode)
{
	llist* node = (llist*)malloc(sizeof(llist));
	node->data = x;
	node->next = nextNode;
	node->prev = prevNode;
	return node;
}

int delete_node(llist* node) 
{
	free(node);
	return node != 0 ? 1 : 0;
}

