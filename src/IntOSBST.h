
#ifndef _INTOSBST_H_
#define _INTOSBST_H_

/* Type defs */

typedef struct _bst_node {
	void* data;				// Takes on void* for flexibility
	struct bst_node* l;
	struct bst_node* r;
} bst_node;

typedef int comp_t(void* l, void* r);

/* Prototypes */

bst_node* 	new_node(void*);
void 		free_node(bst_node*);
bst_node** 	search(bst_node**, comp_t, void*);
void 		insert(bst_node**, comp_t, void*);
void 		delete(bst_node** );

#endif