#include <stdlib.h>
#include <stdio.h>

#include "IntOSBST.h"

/* 
 * IntrinsOS Binary Search Tree (BST) data type
 *
 */

bst_node* new_node(void* data) 
{
	bst_node* result = (bst_node*)malloc(sizeof(bst_node));
	result->data = data;
	result->l = result->r = NULL;	
	return result;
}

void free_node(bst_node* node) 
{
	free(node);
}

bst_node** search(bst_node** root, comp_t compare, void* data) {

	bst_node** node = root;

	/* Iteratively search through entire bst */
	while(*node != NULL) {

		/* Is the node the one we're looking for? */
		int compare_result = compare(data, (*node)->data);

		/* Compare returns -1 for left branch, +1 for right */
		if (compare_result < 0) {
			node = &(*node)->l;
		} else if (compare_result > 0) {
			node = &(*node)->r;
		} else {
			break;
		}
		return node;
	}
}

void insert(bst_node** root, comp_t compare, void* data) {

	/* Find end of the tree */
	bst_node** node = search(root, compare, data);
	
	/* Insert new node */
	if (*node == NULL) {
		*node = new_node(data);
	}
}

void delete(bst_node** node) {
	bst_node* old_node = *node;

	if ((*node)->l == NULL) {
		*node = (*node)->r;
		free_node(old_node);
	} else if ((*node)->r == NULL) {
		*node = (*node)->l;
		free_node(old_node);
	} else {
		bst_node** pred = (*node)->l;
	 	while ((*pred)->r != NULL) {
 	    	pred = (*pred)->r;
 	
 			// Swap
 			void* temp = (*pred)->data;
 			(*pred)->data = (*node)->data;
 			(*node)->data = temp;
 
	 		delete(pred);
	 	}
	}
}	
