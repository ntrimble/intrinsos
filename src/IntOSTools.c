
#include <stdlib.h>

/* Swap two memory locations */

void swap(int* a, int* b) {
    int* tmp = (int*)malloc(sizeof(int*));
    *tmp = *b;
    *b = *a;
    *a = *tmp;
    free(tmp);
}

/* Selection sort algorithm - O(n^2) */

void selectionsort(int** input, int len) {

    int tmp = 0, sj = 0, si = 0, sk = 0;

    /* Scan up rest of array to find new minima */
    while (si < len) {
        sj = si;
        sk = si;
        while (sk < len) {
            if (input[1][sj] > input[1][sk]) {
                sj = sk;
            }
            sk++;
        }

        swap(&input[1][si], &input[1][sj]);
        swap(&input[0][si], &input[0][sj]);
        
        /* Move to next value */
        si++;
    }
}

/* Return size of integer array */
int intLen(int* _in) {
    return (sizeof(_in) / sizeof(int*));
}
