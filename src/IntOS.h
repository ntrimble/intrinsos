#include "IntOSConfig.h"
#include "IntOSDefs.h"

/* Thread */

typedef int (*IO_THREAD_PAYLOAD)(void);

typedef struct _IO_THREAD {

    IO_THREAD_PAYLOAD payload;
    int heapSize;

    enum {
        ONE_SHOT,
        FOREVER
    } persistence;

	enum {
		CRITICAL=1,
 		HIGH,
 		MEDIUM,
 		LOW,
 		TRIVIAL
 	} priority;

 	enum {
 		READY,
 		RUNNING,
 		QUEUED,
 		WAITING,
 		STOPPED
 	} state;

} IO_THREAD;

/* Main RTOS control */

typedef struct _IO_RTOS_STATE {

    int threadCount;
    int runningThreadCount;
    
    IO_THREAD *pThreads[MAX_THREADS];

    enum { 
    	RTOS_STOPPED,
		RTOS_READY,
		RTOS_RUNNING
	} state;

} IO_RTOS_STATE;

/* Prototypes */

int IO_INIT(IO_RTOS_STATE*);
int IO_MAIN(IO_RTOS_STATE*);
IO_THREAD* IO_ADD_THREAD(IO_RTOS_STATE*, int(payload)(void), int, int);
int IO_DELETE_THREAD(IO_RTOS_STATE*, IO_THREAD*);
int IO_THREAD_STATUS(IO_RTOS_STATE*);
int IO_SCHEDULER(IO_RTOS_STATE*);
