/* 
 * IntrinsOS
 * Scheduling RTOS 
 * 
 * Author: nick.trimble@gmail.com
 *
 */

#include "IntOS.h"
#include "IntOSTools.h"
#include "IntOSLList.h"
#include "IntOSBST.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>	
#include <string.h>

/* Vars */


/* System initialisation */

int IO_INIT(IO_RTOS_STATE* _RTOS) {

	IO_THREAD* tempInitThread;

	/* Push all registered threads one step towards execution (WAITING state) */
	int j = 0;
	while (j < _RTOS->threadCount) {
		tempInitThread = _RTOS->pThreads[j];
		tempInitThread->state = WAITING;
		j++;		
	}

	/* Output the number of registered threads and return */
	if (_RTOS->threadCount == 0) {
		printf("[IO_INIT] No registered threads.\n");
		return 1;
	} else if ((_RTOS->threadCount > 0) && (_RTOS->threadCount <= MAX_THREADS)) {
		printf("[IO_INIT] %d registered threads.\n",_RTOS->threadCount);
		return 0;
	} else {
		printf("[IO_INIT] Error counting threads.\n");
		return 1;
	}
}

/* Add thread to RTOS */

IO_THREAD* IO_ADD_THREAD(IO_RTOS_STATE* _RTOS, int(threadTask)(void), int threadPriority, int maxHeapSize) {

	/* Assign memory on heap for thread */
	IO_THREAD* pNewThread = (IO_THREAD*)malloc(sizeof(IO_THREAD)+maxHeapSize);

	/* Assign thread task details */
	pNewThread->priority = threadPriority;
	pNewThread->heapSize = maxHeapSize;
	pNewThread->payload  = threadTask;	
	pNewThread->state    = READY;
	pNewThread->persistence = ONE_SHOT;

	/* Register thread with RTOS */
	_RTOS->pThreads[_RTOS->threadCount] = pNewThread;

	/* Increment thread count */
	_RTOS->threadCount++;

	printf("[IO_ADD_THREAD] New thread at %p with priority %d and heap size %d bytes.\n", pNewThread, threadPriority, maxHeapSize);

	/* Return pointer to thread "object" */
	return pNewThread;
}

/* Delete thread */

int IO_DELETE_THREAD(IO_RTOS_STATE* _RTOS, IO_THREAD* pDelete) {
	
	// TODO Change this to accept threadTask as argument and remove thread based on task alone, not id

	//pDelete = _RTOS->pThreads[j];

	printf("[IO_DELETE_THREAD] Deleting thread at %p.\n", pDelete);

	free(pDelete);

	if (!pDelete) {	/* Memory free()'d successfully */
		_RTOS->threadCount--;
		return 0;
	} else {
		return 1;	/* Couldn't free memory */
	}

}


/* Processor */

int IO_MAIN(IO_RTOS_STATE* _RTOS) 
{
	int i, err;
	IO_THREAD* tempThread;

	IO_SCHEDULER(_RTOS);

process_threads:

	for (i=0;i < _RTOS->threadCount;i++) {

		/* Load first thread into scope */
		tempThread = _RTOS->pThreads[i];

		if (tempThread->state == WAITING) {
			_RTOS->runningThreadCount++;

			/* Change state of thread in scope */
			tempThread->state = RUNNING;

			printf("P%d ", tempThread->priority);

			/* Execute task */
			err = (tempThread->payload)();

			//if (tempThread->persistence == ONE_SHOT) {
			//	tempThread->state = STOPPED;
			//	_RTOS->runningThreadCount--;
				//_RTOS->threadCount--;
			//}

			// TODO need to queue up threads to remove after this pass through of main
			
			/*
			if (tempThread->persistence == ONE_SHOT) {

				// TODO This doesn't work. Segfaults. 

				// De-register single shot threads 
				if (!IO_DELETE_THREAD(_RTOS, tempThread)) {	
					_RTOS->runningThreadCount--;
				} else {
					printf("[IO_DELETE_THREAD] Couldn't delete %p\n", tempThread);
				}
			}
			*/
			
		}
	} 
	
	/* Wait for more to process */
	if (err) {
		/* Panic */
	} else {
		//while(!_RTOS->threadCount);
		//goto process_threads; 
	}

	return 0;
}

/* Print thread status to console */

int IO_THREAD_STATUS(IO_RTOS_STATE* _RTOS) {

	IO_THREAD tempThread;

	for (int j=0;j < _RTOS->threadCount;j++) {

		/* Load thread into scope */
		tempThread = *(_RTOS->pThreads[j]);

		switch(tempThread.state) {
			case READY:
				printf("[IO_STATUS] %p READY.\n", _RTOS->pThreads[j]);
				break;
			case STOPPED:
				printf("[IO_STATUS] %p STOPPED.\n", _RTOS->pThreads[j]);
				break;
			case QUEUED:
				printf("[IO_STATUS] %p QUEUED.\n", _RTOS->pThreads[j]);
				break;
			case WAITING:
				printf("[IO_STATUS] %p WAITING.\n", _RTOS->pThreads[j]);
				break;
			case RUNNING:
				printf("[IO_STATUS] %p RUNNING.\n", _RTOS->pThreads[j]);
				break;
			default: 
				return 1;
				break;
		}
	}
	
	return 0;
}

/* Scheduler */

int IO_SCHEDULER(IO_RTOS_STATE* _RTOS) {

	int i, s = 0;
	IO_THREAD schedTask;

	/* Allocate memory for 2D int array (2 x threadCount) */
	int** sortArray;

	/* Allocate rows */
	sortArray = malloc(2 * sizeof(int*));

	/* Now columns */
	for (i = 0; i < 2; i++) {
		sortArray[i] = malloc(_RTOS->threadCount * sizeof(int*));
	}

	s = 0;
	while (s < _RTOS->threadCount) {

		/* Load next thread into scope */
		schedTask = *(_RTOS->pThreads[s]);

		/* Store its priority against an index */
		sortArray[0][s] = s; // Index
		sortArray[1][s] = schedTask.priority; // Priority

#if 0
		printf("PreSort Index %d Priority %d\n",sortArray[0][s],sortArray[1][s]);
#endif

		s++;
	}

	/* Sort data in order of priority */
	selectionsort(sortArray, _RTOS->threadCount);

#if 1
	s = 0;
	while (s < _RTOS->threadCount) {

		printf("Sorted Index %d Priority %d\n", sortArray[0][s], sortArray[1][s]);
		
		s++;

	}
#endif

	/* Now we know the task number and the order of priority. Now reorder. */

	/* Take a copy of the thread stack */
	IO_RTOS_STATE* pTemp = (IO_RTOS_STATE*)malloc(sizeof(IO_RTOS_STATE)); 
	for (s = 0; s < _RTOS->threadCount; s++) {
		pTemp->pThreads[s] = _RTOS->pThreads[s];
	}

    /* Copy thread order back into _RTOS->pThreads so we have a list to process. */
    for (s = 0; s < _RTOS->threadCount; s++) {
		_RTOS->pThreads[s] = pTemp->pThreads[sortArray[0][s]];     
	}

	/* Free allocated memory after re-arranging */
	free(sortArray);
	free(pTemp);

	if (!pTemp && !sortArray) {
		/* Memory free()'d, success. */
		return 0;
	} else {
		/* Something didn't free */
		return 1;
	}
}

